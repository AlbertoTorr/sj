import Vue from 'vue'
import router from './router'
import App from './App.vue'
import { BootstrapVue } from 'bootstrap-vue'
import VueI18n from 'vue-i18n'
import './vee-validate'
import VueKinesis from 'vue-kinesis'
import store from './store'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
Vue.use(VueKinesis)

Vue.config.productionTip = false

Vue.use(VueI18n)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
