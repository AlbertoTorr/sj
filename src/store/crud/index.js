// import data from '../../db/data.json'
import { datas } from '../../db/data'

export default {
  namespaced: true,
  state: {
    posts: [],
    errors: []
  },
  getters: {
    getPosts (state) {
      return state.posts
    },
    getPostsDB () {
      return [...datas.posts]
    },
    getErrors (state) {
      return state.errors
    }
  },
  mutations: {
    setPosts (state, posts) {
      state.posts = posts
    },
    setPost (state, postEdited) {
      // console.log('statepost prima del set', state.posts)
      const index = datas.posts.findIndex(post => post.id === postEdited.id)
      // console.log('dentro set', index)
      if (index >= 0) datas.posts[index] = { ...postEdited }
      // console.log('dopo il set datapost', datas.posts)
      state.posts[index] = { ...postEdited }
      // console.log('dopo il set statepost', state.posts)
    },
    deletePost (state, postEdited) {
      console.log('dentro store')
      const index = datas.posts.findIndex(post => post.id === postEdited.id)
      console.log('dentro set', index)
      if (index >= 0) datas.posts.slice(index, 1)
      state.posts.splice(index, 1)
      console.log('dopo il set datapost', state.posts)
    },
    loadPosts (state) {
      // console.log('state.posts', state.posts)
      // state.posts = [...data.posts]
      state.posts = [...datas.posts]
    }
  },
  actions: {
    /* login (_, formData) {
      return new Promise((resolve, reject) => {
        axios.post('https://manager.petergoodstrong.com/api/login', {
          email: formData.email,
          password: formData.password
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }, */
    fetchPosts ({ commit, getters }, formData) {
      commit('loadPosts')
      return new Promise((resolve, reject) => {
        if (getters.getPosts.length > 0) {
          resolve(true)
        } else {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject(false)
        }
      })
    }
  }
}
