// import data from '../../db/data.json'
import { datas } from '../../db/data'

export default {
  namespaced: true,
  state: {
    authorized: false,
    errors: [],
    registered: false
  },
  getters: {
    getAuthorized (state) {
      return state.authorized
    },
    getErrors (state) {
      return state.errors
    },
    getRegistered (state) {
      return state.registered
    }
  },
  mutations: {
    setToken () {
      localStorage.setItem('token', datas.token)
    },
    setStorageLogout () {
      localStorage.clear()
    },
    setAuth (state, value) {
      state.authorized = value
    },
    setRegistration (state, form) {
      const user = datas.users.find(el => el.email === form.email)
      // console.log('find user', user)
      if (!user) {
        datas.users.push({ id: datas.users.length + 1, ...form })
        state.registered = true
      }
    },
    checkDataLogin (state, email, password) {
      // console.log(datas)
      // console.log('state.authorized', state.authorized)
      const user = datas.users.find(user => user.email === email || user.password === String(password))
      console.log('user login', user)
      if (user) {
        state.authorized = true
        localStorage.setItem('user', JSON.stringify({ name: user.fullName, email: user.email }))
      }
      state.errors.push('Email o Password errati!')
    }
  },
  actions: {
    /* login (_, formData) {
      return new Promise((resolve, reject) => {
        axios.post('https://manager.petergoodstrong.com/api/login', {
          email: formData.email,
          password: formData.password
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }, */
    login ({ commit, getters }, formData) {
      commit('checkDataLogin', formData.email, formData.password)
      return new Promise((resolve, reject) => {
        if (getters.getAuthorized) {
          commit('setToken')
          resolve(true)
        } else {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject(false)
        }
      })
    },
    logout ({ commit }) {
      commit('setStorageLogout')
      return new Promise((resolve, reject) => {
        if (!localStorage.getItem('token')) {
          commit('setAuth', false)
          resolve(true)
        } else {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject(false)
        }
      })
    },
    register ({ commit, getters }, formData) {
      commit('setRegistration', formData)
      return new Promise((resolve, reject) => {
        if (getters.getRegistered) {
          // commit('setToken')
          // console.log('prima di resolve register', datas)
          resolve(true)
        } else {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject(false)
        }
      })
    }
  }
}
