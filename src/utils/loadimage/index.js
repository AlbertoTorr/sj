export const blobToBase64 = blob => new Promise(resolve => {
  const reader = new FileReader()
  reader.onloadend = () => resolve(reader.result)
  reader.readAsDataURL(blob)
})
