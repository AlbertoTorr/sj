import Vue from 'vue'
import VueRouter from 'vue-router'
import auth from './routes/auth'
import menu from './routes/menu'
import crud from './routes/crud'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', redirect: { name: 'login' } },
    ...auth,
    ...menu,
    ...crud,
    {
      path: '*',
      redirect: 'error-404'
    }
  ]
})

router.beforeEach((to, from, next) => {
  // console.log(to, from)
  console.log(!sessionStorage.getItem('token'))
  if (!localStorage.getItem('token') && (to.name !== 'login' && to.name !== 'register')) {
    return next({ name: 'login' })
  }
  next()
})

router.afterEach((to, from, next) => {

})

export default router
