export default [
  {
    path: '/create',
    name: 'create',
    component: () => import('../../views/crud/Create.vue')
  },
  {
    path: '/read/:id',
    name: 'read',
    component: () => import('../../views/crud/Read.vue')
  }
]
