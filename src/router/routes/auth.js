export default [
  {
    path: '/login',
    name: 'login',
    component: () => import('../../views/auth/Login.vue'),
    // redirectIfLoggedIn: true,
    meta: {
      layout: 'full'
    }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../../views/auth/Registration.vue'),
    // redirectIfLoggedIn: true,
    meta: {
      layout: 'full'
    }
  },
  {
    path: '/error-404',
    name: 'error-404',
    component: () => import('@/views/error/Error404.vue'),
    meta: {
      layout: 'full'
    }
  }
]
