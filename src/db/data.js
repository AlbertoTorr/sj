const date = new Date()

export const datas = {
  token: 'dd5f3089-40c3-403d-af14-d0c228b05cb4',
  users: [
    {
      id: 1,
      fullName: 'Alberto Torrisi',
      password: 'corso13',
      // avatar: require('@/assets/confused-travolta.png'),
      email: 'alberto.torrisi@stevejobs.academy'
    },
    {
      id: 2,
      fullName: 'Jane Doe',
      password: 'admin',
      // avatar: '@/assets/images/avatars/1-small.png',
      email: 'janedoe@academy.com'
    }
  ],
  posts: [
    {
      id: 1,
      title: 'AI',
      message: 'Hello Doctor',
      created: new Date(date.getFullYear(), date.getMonth() + 1, date.getDate()),
      author: 'alberto.torrisi@stevejobs.academy'
    },
    {
      id: 2,
      title: 'Shri',
      message: 'Hello there!',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'alberto.torrisi@stevejobs.academy'

    },
    {
      id: 3,
      title: 'AI',
      message: 'Hope you are well. Today’s discussion shall be on treatment options to manage ...',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'alberto.torrisi@stevejobs.academy'
    },
    {
      id: 4,
      title: '32123',
      message: 'Hope you are well. Today’s discussion shall be on treatment options to manage ...',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'janedoe@academy.com'
    },
    {
      id: 5,
      title: 'A23123I',
      message: 'Hope you are well. Today’s discussion shall be on treatment options to manage ...',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'janedoe@academy.com'
    },
    {
      id: 6,
      title: 'AI',
      message: 'Hello Doctor',
      created: new Date(date.getFullYear(), date.getMonth() + 2, -11),
      author: 'alberto.torrisi@stevejobs.academy'
    },
    {
      id: 7,
      title: 'Shri',
      message: 'Hello there!',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'janedoe@academy.com'
    },
    {
      id: 8,
      title: 'AI',
      message: 'Hope you are well. Today’s discussion shall be on treatment options to manage ...',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'janedoe@academy.com'

    },
    {
      id: 9,
      title: '32123',
      message: 'Hope you are well. Today’s discussion shall be on treatment options to manage ...',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'alberto.torrisi@stevejobs.academy'
    },
    {
      id: 10,
      title: 'A23123I',
      message: 'Hope you are well. Today’s discussion shall be on treatment options to manage ...',
      created: new Date(date.getFullYear(), date.getMonth() + 1, -11),
      author: 'alberto.torrisi@stevejobs.academy'
    }
  ]
}
